# haskell-misc

This is where I put random Haskell files I'm working on while I learn the language. You can think of it as me proudly
displaying "I have a 101-understanding of a grad-level course", or as a living record of my exploration into functional
programming.

To ease compiling, I have `build` to both compile and strip the executable. It uses level-2 optimizations then removes
symbols that aren't critical for execution to further reduce file size.

---

### qsort
Somewhat-naïve implementation of quick sort. Uses the first element of the list as the pivot to demonstrate of Haskell's
elegance. I used `[Int]` instead of `[Integer]` because I wanted to testhow this executable's file size compares to
identical code in Rust. To see the results of that experiment, start [here](https://gitlab.com/toyli/rust-size-test).
