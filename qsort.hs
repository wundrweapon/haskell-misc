import System.Environment (getArgs)
import Text.Read (read)

qsort :: Ord a => [a] -> [a]
qsort [] = []
qsort [x] = [x]
qsort [x, y] = if x <= y then [x, y] else [y, x]
qsort (x:xs) = qsort lt ++ eq ++ qsort gt
    where lt = filter (< x) xs
          eq = [x] ++ filter (== x) xs
          gt = filter (> x) xs

main = do
    argv <- getArgs
    print (qsort (map (read) argv :: [Int]))
